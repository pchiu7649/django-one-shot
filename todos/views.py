from django.shortcuts import render, redirect
from todos.models import TodoItem, TodoList
from todos.forms import TodoForm, TodoItems

# Create your views here.
def todo_list(request):
    todos = TodoList.objects.all()

    context = {
        "todo_list": todos,
    }

    return render(request, "todos/todos.html", context)


def todo_detail(request, id):
    todo_object = TodoList.objects.get(id=id)
    context = {"todolist": todo_object}

    return render(request, "todos/detail.html", context)

#CREATE
def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", id=todolist.id)
    else:
        form = TodoForm()
    context = {
        "form":form
    }
    return render(request,"todos/create.html", context)

#CREATE TODOITEM
def todo_item_create(request):
    if request.method == "POST":
        form = TodoItems(request.POST)
        if form.is_valid():
            todos = form.save()
            return redirect("todo_list_detail", id=todos.list.id)
    else:
        form = TodoItems()
    context = {
        "form":form
    }
    return render(request,"items/create.html", context)

#UPDATE
def todo_list_update(request, id):
    todolist = TodoList.objects.get(id=id)

    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todolists = form.save()
            return redirect("todo_list_detail", id=todolists.id)
    else:
        form = TodoForm()
    context ={
        "form":form,
    }
    return render(request,"todos/create.html", context)

#DELETE
def todo_list_delete(request, id):
    tododelete = TodoList.objects.get(id=id)
    if request.method == "POST":
        tododelete.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")

#UpdateItems
def todo_item_update(request, id):
    todo_object = TodoItem.objects.get(id=id)

    if request.method =="POST":
        form = TodoItems(request.POST, instance=todo_object)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", todo_object.list.id)
    else:
        form = TodoItems(instance=todo_object)

    context = {
        "form":form,
    }

    return render(request, "items/update.html", context)